FROM node:20-alpine3.17

WORKDIR /app

COPY . /app

RUN npm install
RUN npm run build

CMD npm run start:prod
